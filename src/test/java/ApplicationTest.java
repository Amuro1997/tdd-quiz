import com.twuc.webApp.Bag;
import com.twuc.webApp.InvalidTicketException;
import com.twuc.webApp.Storage;
import com.twuc.webApp.Ticket;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {
    @Test
    void shouldGetTheTicketWhenSavingABag(){
        Storage storage = new Storage();
        final Bag bag = new Bag();

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void shouldGetTheTicketWhenSaveNothing(){
        Storage storage = new Storage();
        final Bag nothing = null;

        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void shouldGetTheBagWhenGivingAValidTicketThatIsReceivedWhenSavedTheBag(){
        Storage storage = new Storage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);

        assertSame(bag, storage.retrieve(ticket));
    }

    @Test
    void shouldGetTheFirstBagWhenUsingTheFirstTicketToRetrieve(){
        Storage storage = new Storage();
        Bag expectedBag = new Bag();

        Ticket firstTicket = storage.save(expectedBag);
        storage.save(new Bag());

        Bag retrievedFirstBag = storage.retrieve(firstTicket);

        assertSame(expectedBag, retrievedFirstBag);
    }

    @Test
    void shouldGetAnErrorMessageWhenRetrievingTheBagUsingAnInvalidTicket(){
        Storage empty = new Storage();
        Ticket invalidTicket = new Ticket();

        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, ()->empty.retrieve(invalidTicket));

        assertEquals("Invalid ticket", exception.getMessage());
    }

    @Test
    void shouldGetNothingWhenUsingAValidTicketThatSavedNothing(){
        Storage storage = new Storage();
        Bag nothing = null;
        Ticket ticket = storage.save(nothing);
        assertNull(storage.retrieve(ticket));
    }

    @Test
    void shouldGetATicketWhenSavingABagGivingStorageIsEmptyWith2Capacity(){
        Storage storage = new Storage(2);
        Ticket ticket =storage.save(new Bag());
        assertNotNull(ticket);
    }

    @Test
    void shouldGetErrorMessageWhenSavingTheBagGivingTheStorageIsFull(){
        Storage storage = new Storage(2);
        storage.save(null);
        storage.save(null);
        RuntimeException exception = assertThrows(RuntimeException.class, () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void shouldSaveFirstBagAndGetAnErrorMessageWhenSavingSecondBagGivingAnEmptyStorageWithCapacityOne(){
        Storage storage = new Storage(1);
        assertNotNull(storage.save(new Bag()));

        RuntimeException exception = assertThrows(RuntimeException.class, () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity", exception.getMessage());

    }

    @Test
    void shouldRetrieveOneBagAndSaveAnotherBagGivingAFullStorageWithCapacityOne(){
        //given
        Storage storage = new Storage(1);
        Ticket firstBagTicket = storage.save(new Bag());
        //when
        Bag retrievedBag = storage.retrieve(firstBagTicket);
        Ticket secondBagTicket = storage.save(new Bag());

        //then
        assertNotNull(retrievedBag);
        assertNotNull(secondBagTicket);
    }
}


