package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class Storage {

    Map<Ticket, Bag> storage = new HashMap<>();

    private final int capacity;

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    public Storage(){
        this(Integer.MAX_VALUE);
    }

    public Ticket save(Bag bag){
        if(storage.size() == capacity)
            throw new RuntimeException("Insufficient capacity");
        Ticket ticket = new Ticket();
        storage.put(ticket, bag);
        return ticket;
    }

    public Bag retrieve(Ticket ticket) {
        if(!storage.containsKey(ticket))
            throw new IllegalArgumentException("Invalid ticket");
        Bag bag = storage.get(ticket);
        storage.remove(ticket);
        return bag;
    }
}
